package com.example.drdave.dictionary.Social;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ListActivity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cleveroad.pulltorefresh.firework.FireworkyPullToRefreshLayout;
import com.example.drdave.dictionary.R;

import com.example.drdave.dictionary.Social.adapters.ResultsAdapter;
import com.example.drdave.dictionary.Social.models.Result;
import com.example.drdave.dictionary.Social.models.Tweet;
import com.example.drdave.dictionary.Social.adapters.TweetAdapter;
import com.example.drdave.dictionary.Social.tasks.GetTweetsAsyncTask;

import java.util.ArrayList;
import java.util.List;

import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class Social extends ListActivity implements AdapterView.OnItemSelectedListener {

    public String hashtagSearch = ""; // Empty Search String
    private TwitterFactory twitterFactory;
    private View mainProgressView;
    private Handler refreshTweetsHandler;
    public TextView searchResultText;
    public static String one, two,three,four,five;
    public EditText searchText;
    public ResultsAdapter tweetsAdapter;
    public int refreshInterval = 10;
    private RelativeLayout sRelativeLayout;
    public static Context thisContext = null;

    public RecyclerView recycleList;
    public com.cleveroad.pulltorefresh.firework.FireworkyPullToRefreshLayout mPullToRefresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        initializeTwitterFactory();
        initializeView();
        initializeApp();
        Social.thisContext = this;
    }

    private void initializeTwitterFactory(){
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("7OypRf6xQVxcYnSTLl1co6lHq")
                .setOAuthConsumerSecret("TSS6LOZxPjxHHd1apjT7Oy9sPYlEpGrVILrAmU4QTGbDGZytps")
                .setOAuthAccessToken("843824548688617472-aEzqHjLvuhFhMqUCGRhbDIa71fbEIGp")
                .setOAuthAccessTokenSecret("TLNbSvKzZfV58w1TTYP7F5GIfUPL6ePAjKOfqr0gXquB0");
        twitterFactory = new TwitterFactory(cb.build());
    }

    private void initializeView(){

        searchText = (EditText) findViewById(R.id.search_text);
        sRelativeLayout = (RelativeLayout)findViewById(R.id.sRelativeLayout);
        tweetsAdapter = new ResultsAdapter(this);
        mPullToRefresh = (FireworkyPullToRefreshLayout) findViewById(R.id.pullToRefresh);
        searchText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                hashtagSearch = s.toString();
            }
        });

        mPullToRefresh.setOnRefreshListener(new FireworkyPullToRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doSearch();
            }
        });
    }

    private void initializeApp(){

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            getListView().setVisibility(show ? View.GONE : View.VISIBLE);
            getListView().animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    getListView().setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            mPullToRefresh.setRefreshing(show);

        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mPullToRefresh.setRefreshing(show);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Do nothing
        Result d = (Result)parent.getItemAtPosition(position);
        Snackbar.make(null, "We got this: "+d.Word,Snackbar.LENGTH_SHORT).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public static void showToast(String msg){
        Toast.makeText(Social.thisContext, msg,
                Toast.LENGTH_LONG).show();
    }

    public void UpdateList(List<Result> tweets){
        tweetsAdapter.UpdateAdapter((ArrayList<Result>) tweets);
        setListAdapter(tweetsAdapter);
    }

    private Runnable refreshTweetsRunnable = new Runnable() {
        @Override
        public void run() {
            if(hashtagSearch.isEmpty()){
                UpdateList(new ArrayList<Result>());
                showToast("No results found");
                showProgress(false);
            }else {
                showProgress(true);
                showToast("Refreshing, please wait...");
                GetTweetsAsyncTask getTweetsTask = new GetTweetsAsyncTask(twitterFactory, Social.this);
                getTweetsTask.execute();
            }
        }
    };

    public void viewAllPostsClick(View view){
        sRelativeLayout.setVisibility(View.GONE);
    }

    public void onTwitterCloseClick(View view){
        sRelativeLayout.setVisibility(View.VISIBLE);
    }

    public void doSearch(){
        String search_term = hashtagSearch;
        refreshTweetsHandler = new Handler();
        refreshTweetsHandler.postDelayed(refreshTweetsRunnable, refreshInterval); // Perform Search Right Away
    }
}
