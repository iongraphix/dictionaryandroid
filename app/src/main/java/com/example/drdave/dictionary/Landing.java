package com.example.drdave.dictionary;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.drdave.dictionary.All.All;
import com.example.drdave.dictionary.Social.Social;


public class Landing extends Activity {

    //private Button searchButton;
    Button searchli;
    Spinner mySpinner;
    TextView viewTextView;

    Typeface tf1;

    String currentSpinnerText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        mySpinner = (Spinner) findViewById(R.id.menuSpinner);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(Landing.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.spinner));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mySpinner.setAdapter(myAdapter);

        mySpinner.setOnItemSelectedListener(spinnerSelected);

        searchli = (Button) findViewById(R.id.searchButton);
        searchli.setOnClickListener(searchlistener);

        viewTextView = (TextView) findViewById(R.id.viewTextView);
        tf1 = Typeface.createFromAsset(getAssets(), "Lato-Heavy.ttf");

        viewTextView.setTypeface(tf1);
        searchli.setTypeface(tf1);


    }


    int string_position;
    private Spinner.OnItemSelectedListener spinnerSelected = new Spinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            string_position = position;
            currentSpinnerText = parent.getSelectedItem().toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnClickListener searchlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (currentSpinnerText.equals("All")) {
                Intent intent = new Intent(Landing.this, All.class);
                startActivity(intent);
            }
            else if (currentSpinnerText.equals("Social Trends")) {
                Intent intent = new Intent(Landing.this, Social.class);
                startActivity(intent);
            }
        }
    };



}
