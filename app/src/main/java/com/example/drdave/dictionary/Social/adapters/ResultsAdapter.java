package com.example.drdave.dictionary.Social.adapters;

/**
 * Created by DR.DAVE on 3/20/2017.
 */
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.drdave.dictionary.R;
import com.example.drdave.dictionary.Social.Social;
import com.example.drdave.dictionary.Social.models.Result;
import com.example.drdave.dictionary.Social.models.Tweet;

import java.util.ArrayList;
import java.util.List;

public class ResultsAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<Result> results;

    public ResultsAdapter(Context context) {
        initializeAdapter(context);
    }

    private void initializeAdapter(Context context){
        if(context != null) {
            mInflater = LayoutInflater.from(context);
        }
    }

    public void UpdateAdapter(ArrayList<Result> tweets){
        this.results = tweets;
    }

    /**
     * The number of items in the list is determined by the number of speeches
     * in our array.
     *
     * @see android.widget.ListAdapter#getCount()
     */
    public int getCount() {
        if(results == null){
            return 0;
        }
        return (results.size());
    }

    /**
     * Since the data comes from an array, just returning the index is
     * sufficent to get at the data. If we were using a more complex data
     * structure, we would return whatever object represents one row in the
     * list.
     *
     * @see android.widget.ListAdapter#getItem(int)
     */
    public Object getItem(int position) {
        if(position < 0 || position >= results.size() ){
            return null;
        }
        return results.get(position);
    }

    /**
     * Use the array index as a unique id.
     *
     * @see android.widget.ListAdapter#getItemId(int)
     */
    public long getItemId(int position) {
        if(position < 0 || position >= results.size() ){
            throw new IndexOutOfBoundsException();
        }
        return position;
    }

    /**
     * Make a view to hold each row.
     *
     * @see android.widget.ListAdapter#getView(int, View,
     *      ViewGroup)
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.result_item, null);
        }

        final int pos = position;
        TextView tweetText = (TextView) convertView.findViewById(R.id.tweet_word);
        TextView tweetNumber = (TextView) convertView.findViewById(R.id.tweet_number);
        Result tweet = results.get(position);
        tweetText.setText(tweet.Word);
        tweetNumber.setText(tweet.related_tweets.size()+" tweet(s)");
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do nothing
                final View vx  = v;
                Result d = (Result)getItem(pos);
                Snackbar.make(v, "Showing tweets for : "+d.Word,Snackbar.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(Social.thisContext);
                builder.setTitle("Related Tweets");
                final Tweet[] objarr = d.related_tweets.toArray(new Tweet[0]);
                final ArrayList<String> list = new ArrayList<String>();
                for (Tweet t :
                        objarr) {
                    list.add(t.Status);
                }
                builder.setItems(list.toArray(new String[0]), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Snackbar.make(vx, "Opening "+objarr[which].UserName+"'s tweet",Snackbar.LENGTH_SHORT).show();
                        Intent intent = null;
                        String formatted_url = "https://twitter.com/iyke/status/"+objarr[which].TID;
                        try {
                            // get the Twitter app if possible
                            Social.thisContext.getPackageManager().getPackageInfo("com.twitter.android", 0);
                            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=USERID"));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        } catch (Exception e) {
                            // no Twitter app, revert to browser
                            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(formatted_url));
                        }
                        Social.thisContext.startActivity(intent);
                    }
                });
                builder.show();
            }
        });
        return convertView;
    }
}
