package com.example.drdave.dictionary.Social.tasks;

/**
 * Created by DR.DAVE on 3/20/2017.
 */
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.drdave.dictionary.Social.Social;
import com.example.drdave.dictionary.Social.models.Result;
import com.example.drdave.dictionary.Social.models.Tweet;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class GetTweetsAsyncTask extends AsyncTask<String, Void, Boolean> {

    private TwitterFactory twitterFactory;
    private Social social;
    private List<Tweet> tweets;
    public ArrayList<Result> tmpRes;
    public static String[] all_post_texts = new String[10];
    int c = 0;

    AlertDialog commonWordsDisplay;

    public GetTweetsAsyncTask(TwitterFactory twitterFactory, Social social){
        this.twitterFactory = twitterFactory;
        this.social = social;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        try{
            getTweets();
            return true;
        } catch (Exception e) {
            System.out.println("\nError while calling service");
            System.out.println(e);
        }
        return false;
    }

    @Override
    protected void onPostExecute(final Boolean success) {

        social.showProgress(false);
        if (success) {
            social.UpdateList(tmpRes);
            this.social.mPullToRefresh.setRefreshing(false);
        }else{
            social.searchResultText.setText("Uh-oh! Looks like something went wrong searching. Please try again later.");
            this.social.mPullToRefresh.setRefreshing(false);
        }
    }

    @Override
    protected void onCancelled() {
        social.showProgress(false);
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            return null;
        }
    }

    private static <K, V> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
        Collections.sort(list, new Comparator<Object>() {
            @SuppressWarnings("unchecked")
            public int compare(Object o1, Object o2) {
                return ((Comparable<V>) ((Map.Entry<K, V>) (o1)).getValue()).compareTo(((Map.Entry<K, V>) (o2)).getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (Iterator<Map.Entry<K, V>> it = list.iterator(); it.hasNext();) {
            Map.Entry<K, V> entry = (Map.Entry<K, V>) it.next();
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    // TODO: Rewrite the logic in this area
    public void array_analyser(){
        tmpRes = new ArrayList<>();
        String stopWordsTexts = ".,!,?,a,a’s,able,about,above,according,accordingly,across,actually,after,afterwards,again,against,ain’t,all,allow,allows,almost,alone,along,already,also,although,always,am,among,amongst,an,and,another,any,anybody,anyhow,anyone,anything,anyway,anyways,anywhere,apart,appear,appreciate,appropriate,are,aren’t,around,as,aside,ask,asking,associated,at,available,away,awfully,be,became,because,become,becomes,becoming,been,before,beforehand,behind,being,believe,below,beside,besides,best,better,between,beyond,both,brief,but,by,c’mon,c’s,came,can,can’t,cannot,cant,cause,causes,certain,certainly,changes,clearly,co,com,come,comes,concerning,consequently,consider,considering,contain,containing,contains,corresponding,could,couldn’t,course,currently,definitely,described,despite,did,didn’t,different,do,does,doesn’t,doing,don’t,done,down,downwards,during,each,edu,eg,eight,either,else,elsewhere,enough,entirely,especially,et,etc,even,ever,every,everybody,everyone,everything,everywhere,ex,exactly,example,except,far,few,fifth,first,five,followed,following,follows,for,former,formerly,forth,four,from,further,furthermore,get,gets,getting,given,gives,go,goes,going,gone,got,gotten,greetings,had,hadn’t,happens,hardly,has,hasn’t,have,haven’t,having,he,he’s,hello,help,hence,her,here,here’s,hereafter,hereby,herein,hereupon,hers,herself,hi,him,himself,his,hither,hopefully,how,howbeit,however,i,I,i’d,i’ll,i’m,i’ve,ie,if,ignored,immediate,in,inasmuch,inc,indeed,indicate,indicated,indicates,inner,insofar,instead,into,inward,is,isn’t,it,it’d,it’ll,it’s,its,itself,just,keep,keeps,kept,know,knows,known,last,lately,later,latter,latterly,least,less,lest,let,let’s,like,liked,likely,little,look,looking,looks,ltd,mainly,many,may,maybe,me,mean,meanwhile,merely,might,more,moreover,most,mostly,much,must,my,myself,name,namely,nd,near,nearly,necessary,need,needs,neither,never,nevertheless,new,next,nine,no,nobody,non,none,noone,nor,normally,not,nothing,novel,now,nowhere,obviously,of,off,often,oh,ok,okay,old,on,once,one,ones,only,onto,or,other,others,otherwise,ought,our,ours,ourselves,out,outside,over,overall,own,particular,particularly,per,perhaps,placed,please,plus,possible,presumably,probably,provides,que,quite,qv,rather,rd,re,really,reasonably,regarding,regardless,regards,relatively,respectively,right,said,same,saw,say,saying,says,second,secondly,see,seeing,seem,seemed,seeming,seems,seen,self,selves,sensible,sent,serious,seriously,seven,several,shall,she,should,shouldn’t,since,six,so,some,somebody,somehow,someone,something,sometime,sometimes,somewhat,somewhere,soon,sorry,specified,specify,specifying,still,sub,such,sup,sure,t’s,take,taken,tell,tends,th,than,thank,thanks,thanx,that,that’s,thats,the,their,theirs,them,themselves,then,thence,there,there’s,thereafter,thereby,therefore,therein,theres,thereupon,these,they,they’d,they’ll,they’re,they’ve,think,third,this,thorough,thoroughly,those,though,three,through,throughout,thru,thus,to,together,too,took,toward,towards,tried,tries,truly,try,trying,twice,two,un,under,unfortunately,unless,unlikely,until,unto,up,upon,us,use,used,useful,uses,using,usually,value,various,very,via,viz,vs,want,wants,was,wasn’t,way,we,we’d,we’ll,we’re,we’ve,welcome,well,went,were,weren’t,what,what’s,whatever,when,whence,whenever,where,where’s,whereafter,whereas,whereby,wherein,whereupon,wherever,whether,which,while,whither,who,who’s,whoever,whole,whom,whose,why,will,willing,wish,with,within,without,won’t,wonder,would,would,wouldn’t,yes,yet,you,you’d,you’ll,you’re,you’ve,your,yours,yourself,yourselves,zero";
        String[] stopWords = stopWordsTexts.toUpperCase().split(",");
        List<Tweet> tempTweets = tweets;
        LinkedList<String> all_important_words = new LinkedList<String>();
        LinkedList<String> stopWordsList = new LinkedList<String>(Arrays.asList(stopWords));
        for (Tweet tweet : tempTweets) {
            String tweet_content = tweet.Status.toUpperCase(); // For Equality
            String[] tweet_words = tweet_content.split(" "); // Break em up
            LinkedList<String> tweet_words_list =  new LinkedList<String>(Arrays.asList(tweet_words));
            tweet_words_list.removeAll(stopWordsList); // Prune the tweets with Stopwords
            removeTwitterSyntax(tweet_words_list);
            all_important_words.addAll(tweet_words_list);
        }
        HashMap<String,Integer> ranking = rankWords(all_important_words);
        Map<String, Integer> sorted_ranking = sortByValue(ranking);
        for (Map.Entry<String, Integer> entry : sorted_ranking.entrySet())
        {
            Result r = new Result();
            r.Word = entry.getKey();
            r.related_tweets = getRelatedTweets(r.Word);
            tmpRes.add(r);
        }

    }

    private ArrayList<Tweet> getRelatedTweets(String word) {
        ArrayList<Tweet> tmp = new ArrayList<>();
        for (Tweet t : tweets) {
            if (t.Status.toUpperCase().contains(word.toUpperCase())) {
                tmp.add(t);
            }
        }
        return tmp;
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    private void removeTwitterSyntax(LinkedList<String> tweet_words_list) {
        for (Iterator<String> iterator = tweet_words_list.iterator(); iterator.hasNext();) {
            String word = iterator.next().toLowerCase();
            // Test Regex: for @ghana or #ghana or url
            if(word.matches("\\S*(#|@)(?:\\[[^\\]]+\\]|\\S+)") || word.matches("(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]\\.[^\\s]{2,})")){
                iterator.remove();
            }
            // Check if its a number
            else if (isNumeric(word)) {
                iterator.remove();
            }
            // Check if empty
            else if(word.isEmpty()){
                iterator.remove();
            }
            // Contains UniCode Characters
            else if(isEncoded(word)){
                iterator.remove();
            }
        }
    }

    public boolean isEncoded(String text){

        Charset charset = Charset.forName("US-ASCII");
        String checked=new String(text.getBytes(charset),charset);
        return !checked.equals(text);

    }

    private HashMap<String,Integer> rankWords(List<String> tweet_words_list) {
        HashMap<String, Integer> occurrences = new HashMap<String, Integer>();

        for ( String word : tweet_words_list ) {
            Integer oldCount = occurrences.get(word);
            if ( oldCount == null ) {
                oldCount = 0;
            }
            occurrences.put(word, oldCount + 1);
        }
        return occurrences;
    }


    private void getTweets(){
        String hashtagSearch = social.hashtagSearch;

        if(!hashtagSearch.contains("#")){
            hashtagSearch = "#"+ hashtagSearch;
        }

        Twitter twitter = twitterFactory.getInstance();
        try {
            Query query = new Query(hashtagSearch);
            QueryResult result = twitter.search(query);
            tweets = new ArrayList<Tweet>();
            for (twitter4j.Status status : result.getTweets()) {
                Tweet tweet = new Tweet();
                tweet.UserName = status.getUser().getName();
                tweet.PostDate = status.getCreatedAt().toString();
                tweet.Status = status.getText();
                tweet.TID = status.getId();
                tweets.add(tweet);
            }
        } catch (TwitterException te) {
            te.printStackTrace();
        }
        array_analyser();
    }
}

