package com.example.drdave.dictionary.Social.models;

import java.util.ArrayList;

/**
 * Created by dominic on 5/13/17.
 */
public class Result {
    public String Word;
    public ArrayList<Tweet> related_tweets;
}
