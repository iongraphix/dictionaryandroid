package com.example.drdave.dictionary.All;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.drdave.dictionary.R;

public class All extends Activity {

    WebView brow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all);

        brow = (WebView) findViewById(R.id.webView);
        brow.setWebViewClient(new DictBrow());
        brow.getSettings().setJavaScriptEnabled(true);
        brow.loadUrl("http://192.168.137.1/dictionary/");

    }

    private class DictBrow extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
